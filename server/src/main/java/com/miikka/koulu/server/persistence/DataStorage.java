package com.miikka.koulu.server.persistence;

import java.util.List;

import com.miikka.koulu.server.persistence.model.Message;

public interface DataStorage {
	public void addMessage(Message message);
	public List<Message> getMessages();
	public Message getMessage(long messageId);
}
