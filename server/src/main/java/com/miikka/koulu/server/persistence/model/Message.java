package com.miikka.koulu.server.persistence.model;

import org.joda.time.DateTime;


public class Message {
    
    private long id;
	private String message;
	private DateTime datetime;
	
	public Message(String message, DateTime datetime) {
	    this.message = message;
	    this.datetime = datetime;
    }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

    public DateTime getDateTime() {
        return datetime;
    }

    public void setDateTime(DateTime datetime) {
        this.datetime = datetime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
