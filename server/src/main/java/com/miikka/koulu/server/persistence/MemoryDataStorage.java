package com.miikka.koulu.server.persistence;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import com.miikka.koulu.server.persistence.model.Message;



public class MemoryDataStorage implements DataStorage {

    private volatile static long idCounter = 1;
    
    private LinkedBlockingQueue<Message> messages;

    public MemoryDataStorage(int capacity) {
        messages = new LinkedBlockingQueue<Message>(capacity);
    }

    public synchronized void addMessage(Message message) {
        message.setId(idCounter++);
        if (!messages.offer(message)) {
            messages.poll();
            messages.offer(message);
        }
    }

    public List<Message> getMessages() {
        return new ArrayList<Message>(messages);
    }

    public Message getMessage(long messageId) {
        for (Message msg : messages) {
            if (msg.getId() == messageId) {
                return msg;
            }
        }
        return null;
    }
}
