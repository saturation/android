package com.miikka.koulu.server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.miikka.koulu.server.persistence.DataStorage;
import com.miikka.koulu.server.persistence.MemoryDataStorage;
import com.miikka.koulu.server.rest.MessageServlet;

public class ChatServer {
    
    public static final int SERVER_PORT = 8080;
    public static final int MESSAGE_CAPACITY = 10;
    public static final Logger log = LoggerFactory.getLogger(ChatServer.class);
    
    public static void main(String[] args) throws Exception {

        
        DataStorage dataStorage = new MemoryDataStorage(MESSAGE_CAPACITY);
        
        Server server = new Server(SERVER_PORT);
        log.info("Server started on port: {}", SERVER_PORT);
        
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        context.addServlet(new ServletHolder(new MessageServlet(dataStorage)), "/api");
        server.start();
        server.join();
        
    }
}
