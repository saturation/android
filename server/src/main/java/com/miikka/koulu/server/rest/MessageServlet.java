package com.miikka.koulu.server.rest;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.miikka.koulu.server.persistence.DataStorage;
import com.miikka.koulu.server.persistence.model.Message;
import com.miikka.koulu.server.util.DateTimeTypeConverter;

@javax.servlet.annotation.WebServlet(asyncSupported = true)
public class MessageServlet extends HttpServlet {

    private static final long serialVersionUID = -6035887080474590557L;
    private static DataStorage dataStorage;
    private static final Logger log = LoggerFactory.getLogger(MessageServlet.class);
    private static final Gson gson = new GsonBuilder().registerTypeAdapter(DateTime.class, new DateTimeTypeConverter()).create();
    
    public MessageServlet(DataStorage storage) {
        dataStorage = storage;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json");

        PrintWriter out = response.getWriter();
        out.write(gson.toJson(dataStorage.getMessages()));
        out.flush();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String messageInput = request.getParameter("message");

        if(!StringUtils.isEmpty(messageInput)) {
            log.debug("raw message is: {}", messageInput);
            Message message = gson.fromJson(messageInput, Message.class);
            message.setDateTime(new DateTime());
            dataStorage.addMessage(message);
        }
    }

}
