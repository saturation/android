OAMK Harjoitteluprojekti

yleinen vaatimusmäärittely
==========================
Selkeä ja yksinkertainen, mutta tehokas keskusteluohjelma. Ohjelmassa käyttäjä voi android-asiakasohjelmistosta lähettää viestejä palvelimelle muiden vastaavan ohjelman käyttäjien luettavaksi, sekä katsoa muiden käyttäjien lähettämiä viestejä.
Ensimmäisissä versioissa kaikki näkevät kaikkien viestit ja kuka tahansa voi liittyä keskusteluun, sekä lähettää sinne viestejä.
Protokollana tulee olla http:n päälle rakennettu, sillä se on helposti otettavissa käyttöön muilla alustoilla(iphone, selaimet, windows, symbian ja käytännössä kaikille ohjelmointikielille löytyy helposti hyödynnettävissä olevia kirjastoja). Välitettävät viestit on serialisoitu json objekteiksi. Käytettävät rajapinnat tulee perustua avoimiin standardeihin. Asiakasohjelmisto ottaa yhteyttä palvelimeen säännöllisin väliajoin(pollaus) tarkastaakseen onko uusia viestejä tullut palveluun luettavaksi. Samoin viestin lähetyksessä asiakasohjelmisto ottaa yhteyttä palvelimeen tekemällä "POST"-kutsun. 



java palvelinohjelmisto
=======================
tehty     - maven projektin luonti
tehty     - tarvittavien kirjasto riippuvuuksien(maven dependecy) liittäminen(jetty,gson)
tekemättä - viestien tallennus tietokantaan(sqlite, mysql, postgres)(hibernate)
kesken    - servelettien kirjoittaminen
tehty     - tee servletistä asynkrooninen(non-blockin io)(ei käytännön merkitystä)
tehty     - käyttöönotto scripta(maven?)
tekemättä - kuvan vastaanotto
tekemättä - kuvan lähettäminen edelleen muille asiakkailla
tekemättä - mahdollisuus rekisteröityä
tekemättä - keskusteluhuoneet
tekemättä - kahdenkeskinen keskustelu
tekemättä - tuki nimimerkille
tekemättä - konfiguroitavat parametrit(portti jne.)
tekemättä - "war" jakelu serveristä WEB-INF/web.xml
tekemättä - "ear" jakelu serveristä onko tarpeen?
kesken    - paranna lokitusta
tekemättä - vaihda lokituksen tasoa pienemmälle("vähemmän ääntä") logback.xml?
tehty	  - komentorivi käännös mavenilla ja classpath kuntoon
tekemättä - tehdas suunnittelumalli REST-pisteiden luonnille, jos käytössä servlet
tekemättä - enterprise arkkitehtuuri?
tekemättä - korkea tavoitettavuus(High Availability, HA)
tekemättä - kuormantasaus
tehty     - vastaanota json-objekti puhtaan query-parametrin sijasta
tehty     - Message.java - lisää aikaa kuvaava jäsenmuuttuja
tekemättä - ota käyttöön restlet/jersey/resteasy tms. rest-ohjelmistokehys joilla "korvata" servletit
tekemättä - käytä muistitietokantaa MemoryDataStorage.javan tilalla(esim. H2)
tekemättä - java poikkeuksien liittäminen http tilakoodeihin(InvalidArgumentException -> 404?) 
tekemättä - tietoturva, tarkasta syötteet, rajoita viestien pituus järkeväksi
tekemättä - tee enemmän tarkastuksia syötteille ja ota kiinni mahdolliset virheet parsinnassa 
tekemättä - 



android asiakasohjelmisto
=========================
tehty     - maven android projekti
tehty     - projektin runko
tehty     - kielistys, tee strings.xml
tehty     - käyttöliittymän suunnittelu
tehty     - viestin näyttäminen
tehty     - viestien lähetys palvelimelle
tehty     - viestien vastaan ottaminen palvelimelta
tekemättä - mahdollisuus valita nimimerkki
tekemättä - mahdollisuus valita palvelin
tekemättä - kuvan lähettäminen palveluun
tekemättä - kuvan vastaanottaminen
tekemättä - ohjelman obfuskointi(proguard)
tekemättä - rekisteröityminen
tekemättä - kahdenkeskinen keskustelu
tekemättä - keskusteluhuoneet
tekemättä - omien asetusten tallentaminen
tekemättä - vanhojen keskustelujen näyttäminen, loki
kesken    - pohdi onko käyttöliittymä-säikeen(ChatClient.java) ja keskustelu-säikeen(ChatThread.java) välinen kommunikointi fiksua
tekemättä - tuki https:lle 
tekemättä - käytä laitteelle asetettuja verkkoasetuksia?
tekemättä - tarkasta onko verkko käytettävissä
tekemättä - tee enemmän tarkastuksia syötteille ja ota kiinni mahdolliset virheet parsinnassa 
tekemättä - käytä ScheduledThreadPoolExecutor-luokkaa TimerTask ja Timer -luokkien tilalla, ehkä?
tekemättä - tausta android Service 
tekemättä - notifikaatiot ja toastit
tekemättä - refaktoroi NetworkUtil.java



yhteiset palvelin- ja asiakasohjelmistolle
==========================================
tehty     - video esittely ohjelmasta
tehty     - projektin verkkosivut
tekemättä - rest rajapinta
tekemättä - autentikointi ja auktorisointi 
tekemättä - Message.java nimeäminen ChatMessage.java:ski
tekemättä - yhteiset tietomallit esim. ChatMessage.java, oma maven moduuli jonka voi lisätä riippuvuutena molemmille projekteille
tekemättä - projektin kaikkien rakentaminen alusta loppuun yhdellä käskyllä
tekemättä - liikenteen salaus
tekemättä - yksikkötestaus
tekemättä - systeemitestaus
tekemättä - tee ohjelmasta vikasietoisempi
tehty     - luokkaaviosta kuva
tehty     - dokumentointi
tekemättä - selainkäyttöinen asiakasohjelma(javascript)
tekemättä - jatkuva integraatio(continuous integration/ci)(jenkins)
tekemättä - jatkuva jakelu(continuous deployment)(jenkins?)
tekemättä - palvelin- ja asiakasohjelmistoista maven moduuleita, samoin tietomalleista jotka jaetaan edellisten kesken
tekemättä - staattinen jne. koodianalysointi
kesken    - pidä huolta, että systeemin komponentit seuraavat ympäristöille ominaisia ohjelmointikäytäntöjä
tehty     - javadoc
kesken    - koodin refaktorointi, ei koskaan valmis















