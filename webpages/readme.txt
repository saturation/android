LUE MINUT
=========
projekti käyttää seuraavia työkaluja:
Apache Maven 3.0.4
Oracle Java HotSpot 64-Bit 1.7
Google Android SDK 20.0.3
Eclipse SDK 4.2.1
Git 1.8.0

PROJEKTIN KLOONAUS BITBUCKETISTA
================================
git clone git@bitbucket.org:saturation/android.git

PALVELIMEN KÄÄNNÖS KOMENTORIVILTÄ
=================================
kirjoita server -hakemistossa missä pom.xml sijaitsee
$ mvn clean install

PALVELIMEN KÄYNNISTÄMINEN KOMENTORIVILTÄ
========================================
$ java -jar target/server-1.0-SNAPSHOT.jar

PALVELIN- JA ASIAKASPROJEKTIN AVAAMINEN ECLIPSELLÄ
==================================================
edit -> import -> maven -> existing maven projects -> "browse" kohtaa etsit hakemiston jossa pom.xml sijaitsee

ANDROID EMULAATTORIN KÄYNNISTYS
===============================
mvn android:emulator-start
tai
emulator -avd Default -no-boot-anim -no-audio -wipe-data

ANDROID ASIAKASOHJELMISTON KÄÄNNÖS KOMENTORIVILTÄ JA SIIRTO EMULAATTORIIN
=========================================================================
varmista, että sinulla on lisättynä emulaattorissa Default nimellä oleva AVD(Android Virtual Device)
aseta ympäristömuuttuja ANDROID_HOME osoittamaan android sdk:n esimerkiksi:
export ANDROID_HOME=/home/miikka/dev/android-sdk-linux/
mvn clean install android:deploy













