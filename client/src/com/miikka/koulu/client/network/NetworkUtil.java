package com.miikka.koulu.client.network;

import static com.miikka.koulu.client.BuildConfig.DEBUG;
import static com.miikka.koulu.client.Constants.TAG;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.joda.time.DateTime;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.miikka.koulu.client.DateTimeTypeConverter;
import com.miikka.koulu.client.Message;


public class NetworkUtil {

    private static final Gson gson = new GsonBuilder().registerTypeAdapter(DateTime.class, new DateTimeTypeConverter()).create();

    
    public static void sendMessage(String url, String message) {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost method;
        try {
            method = new HttpPost(new URI(url));
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("message",gson.toJson(new Message(message))));
            method.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            if (DEBUG) {
                Log.i(TAG, "sending message" + method.getEntity().toString() + " to " + url);
            }
            httpClient.execute(method);
            
        } catch (Exception ex) {
            if (DEBUG) {
                Log.e(TAG, "sending message failed.", ex);
            }
        }
    }

    public static List<Message> getMessages(String url) {
        String data = getJSONData(url);        
        Type collectionType = new TypeToken<Collection<Message>>() {}.getType();
        List<Message> messages = new ArrayList<Message>();
        try {
            messages = gson.fromJson(data, collectionType);
        } catch (Exception ex) {
            if (DEBUG) {
                Log.e(TAG, "data parsing failed.", ex);
            }
        }
        return messages;
    }

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
    
    public static String getJSONData(String url) {

        HttpClient httpClient = new DefaultHttpClient();
        String json = "";

        try {
            URI uri = new URI(url);
            if (DEBUG) {
                Log.i(TAG, "fetching data from " + uri.toString());
            }
            HttpGet method = new HttpGet(uri);
            HttpResponse response = httpClient.execute(method);
            InputStream data = response.getEntity().getContent();
            json = convertStreamToString(data);
            if (DEBUG) {
                Log.v(TAG, "received following json: " + json);
            }
        } catch (Exception ex) {
            if (DEBUG) {
                Log.e(TAG, "fetching data failed", ex);
            }
        }

        return json;
    }
}
