package com.miikka.koulu.client;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import static com.miikka.koulu.client.BuildConfig.DEBUG;
import static com.miikka.koulu.client.Constants.TAG;

public class ChatClient extends Activity {
    
    public static final int TEXT_MESSAGE = 0; 
    public static final int IMAGE_MESSAGE = 1;  
    public static final int FETCH_MESSAGES = 3;

    private Button mSendButton;
    private ListView mMessageView;

    private ChatThread mChatThread;
    private static ArrayAdapter<String> mMessgeAdapter;
    
    private Handler mChatHandler;
    private Timer mTimer;
    private TimerTask mTask;
    
    final static private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
            case TEXT_MESSAGE:
                /*
                String textMessage = (String) msg.obj;
                mMessgeAdapter.add("Me:  " + textMessage);
                */
                break;
            case IMAGE_MESSAGE:
                break;
            case FETCH_MESSAGES:
                mMessgeAdapter.clear();
                for(com.miikka.koulu.client.Message message : (ArrayList<com.miikka.koulu.client.Message>) msg.obj) {
                    mMessgeAdapter.add(message.getMessage());
                }
                break;
            }   
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mSendButton = (Button) findViewById(R.id.sendButton);
        mSendButton.setOnClickListener(new SendButtonClickListener());
        
        mMessgeAdapter = new ArrayAdapter<String>(this, R.layout.message_layout);
        mMessageView = (ListView) findViewById(R.id.messageList);
        mMessageView.setAdapter(mMessgeAdapter);
        
        mChatThread = new ChatThread(mHandler);
        mChatThread.start();
        mChatHandler = mChatThread.getHandler(); 
        
        mTask = new TimerTask() {
            public void run() {
                if(mChatHandler != null) {
                    if(DEBUG) {
                        Log.i(TAG, "timer run");
                    }
                    mChatHandler.obtainMessage(ChatClient.FETCH_MESSAGES).sendToTarget();
                }
            }
        };
        mTimer = new Timer();
        mTimer.schedule(mTask, 6000, 6000);
    }
    
    @Override
    protected void onDestroy() {
        mTimer.cancel();
        mChatThread.stop();
        super.onDestroy();
    }

    private class SendButtonClickListener implements OnClickListener {
        public void onClick(View source) {
            
            //if we want to implement other actions / for future refactoring..
            if(source.getId() != R.id.sendButton) {
                return;
            }

            TextView editText = (TextView) findViewById(R.id.editMessage);
            String message = editText.getText().toString();
            editText.getEditableText().clear();
            if (message.length() == 0) {
                return;
            }
            
            mChatHandler.obtainMessage(ChatClient.TEXT_MESSAGE, message).sendToTarget();
            
        }
    }

}