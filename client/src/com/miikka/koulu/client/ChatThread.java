package com.miikka.koulu.client;

import java.util.List;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.miikka.koulu.client.network.NetworkUtil;

public final class ChatThread extends Thread {

    private static Handler mOutputHandler;
    private static Handler mHandler;

    public ChatThread(Handler handler) {
        mOutputHandler = handler;
        
        mHandler = new ChatThreadHandler();

    }
    
    private static class ChatThreadHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {

                case ChatClient.TEXT_MESSAGE:
                    String textMessage = (String) msg.obj;
                    NetworkUtil.sendMessage("http://10.0.1.1:8080/api", textMessage);
                    //mOutputHandler.obtainMessage(ChatClient.TEXT_MESSAGE, textMessage).sendToTarget();
                    break;

                case ChatClient.IMAGE_MESSAGE:
                    break;

                case ChatClient.FETCH_MESSAGES:
                    List<com.miikka.koulu.client.Message> messages = NetworkUtil.getMessages("http://10.0.1.1:8080/api");
                    mOutputHandler.obtainMessage(ChatClient.FETCH_MESSAGES, messages).sendToTarget();
                    break;
            }
        }
        
    }

    @Override
    public void run() {
        Looper.prepare();

        Looper.loop();
    }

    public Handler getHandler() {
        return mHandler;
    }

}
