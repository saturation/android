package com.miikka.koulu.client;



import java.util.Date;

import org.joda.time.DateTime;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class DateTimeTypeConverter implements JsonSerializer<DateTime>, JsonDeserializer<DateTime> {

    public DateTime deserialize(JsonElement json, java.lang.reflect.Type type, JsonDeserializationContext context) throws JsonParseException {
        try {
            return new DateTime(json.getAsString());
        } catch (IllegalArgumentException e) {
            Date date = context.deserialize(json, Date.class);
            return new DateTime(date);
        }
    }

    public JsonElement serialize(DateTime src, java.lang.reflect.Type srcType, JsonSerializationContext context) {
        return new JsonPrimitive(src.toString());
    }
}